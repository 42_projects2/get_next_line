/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/25 16:50:44 by tfriedri          #+#    #+#             */
/*   Updated: 2022/04/28 18:29:40 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_strchr(const char *s, int c)
{
	int	i;

	i = 0;
	while (s[i] != (char)c && s[i] != '\0')
		i++;
	if (s[i] == (char)c)
		return ((char *)(s + i));
	return (NULL);
}

void	*ft_calloc(size_t count, size_t size)
{
	void	*rtp;
	size_t	n;

	rtp = malloc(count * size);
	if (rtp != NULL)
	{
		n = count * size;
		while (n > 0)
		{
			n--;
			((char *)rtp)[n] = '\0';
		}
	}
	return (rtp);
}

size_t	ft_strlen(const char *str)
{
	size_t	i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		l1;
	int		l2;
	char	*rstr;
	size_t	n;

	if (!s1 || !s2)
		return (NULL);
	l1 = ft_strlen(s1);
	l2 = ft_strlen(s2);
	rstr = (char *)malloc(sizeof(char) * (l1 + l2 + 1));
	if (!rstr)
		return (NULL);
	n = l1;
	while (n-- > 0 && (rstr || s1))
		((unsigned char *)rstr)[n] = ((unsigned char *)s1)[n];
	n = l2;
	while (n-- > 0 && ((rstr + l1) || s2))
		((unsigned char *)(rstr + l1))[n] = ((unsigned char *)s2)[n];
	rstr[l1 + l2] = '\0';
	return (rstr);
}

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t	cpysize;
	size_t	n;

	if (dstsize > ft_strlen(src))
		cpysize = ft_strlen(src);
	else
		cpysize = dstsize - 1;
	if (dstsize != 0)
	{
		n = cpysize;
		while (n > 0 && (dst || src))
		{
			n--;
			((unsigned char *)dst)[n] = ((unsigned char *)src)[n];
		}
		*(dst + cpysize) = '\0';
	}
	return (ft_strlen(src));
}
