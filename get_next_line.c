/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/27 09:56:39 by tfriedri          #+#    #+#             */
/*   Updated: 2022/05/03 10:06:16 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*read_buff(int fd, char *buff, int *end_bool)
{
	ssize_t	r_size;

	if (buff)
		return (buff);
	buff = ft_calloc(BUFFER_SIZE + 1, sizeof(char));
	if (!buff)
		return (NULL);
	r_size = read(fd, buff, BUFFER_SIZE);
	if (r_size < BUFFER_SIZE && ft_strchr(buff, 10) == NULL)
		*end_bool = 1;
	if (r_size == 0)
	{
		free(buff);
		return (NULL);
	}
	return (buff);
}

char	*read_and_join(int fd, char *buff, int *end_bool)
{
	char	*buff2;
	char	*new_buff;
	ssize_t	r_size;

	buff2 = ft_calloc(BUFFER_SIZE + 1, sizeof(char));
	if (!buff2)
		return (NULL);
	r_size = read(fd, buff2, BUFFER_SIZE);
	if (r_size < BUFFER_SIZE && ft_strchr(buff2, 10) == NULL)
	{
		if (ft_strlen(buff) == 0 && ft_strlen(buff2) == 0)
		{
			free(buff);
			free(buff2);
			return (NULL);
		}
		*end_bool = 1;
	}
	new_buff = ft_strjoin(buff, buff2);
	free(buff);
	free(buff2);
	if (!new_buff)
		return (NULL);
	return (new_buff);
}

char	*get_new_buff(char *buff, char **ret_str)
{
	char	*new_buff;

	new_buff = ft_strjoin("", ft_strchr(buff, 10) + 1);
	if (!new_buff)
		return (NULL);
	ft_strchr(buff, 10)[1] = '\0';
	*ret_str = ft_strjoin("", buff);
	free(buff);
	return (new_buff);
}

char	*get_next_line(int fd)
{
	static char	*buff;
	char		*ret_str;
	int			end_bool;

	end_bool = 0;
	if (BUFFER_SIZE <= 0 || fd < 0 || read(fd, 0, 0) < 0 || end_bool == 1)
		return (NULL);
	buff = read_buff(fd, buff, &end_bool);
	while (buff != NULL && end_bool == 0 && ft_strchr(buff, 10) == NULL)
		buff = read_and_join(fd, buff, &end_bool);
	if (!buff)
		return (NULL);
	if (ft_strchr(buff, 10) != NULL)
		buff = get_new_buff(buff, &ret_str);
	else
		ret_str = buff;
	if (!buff)
		return (NULL);
	if (end_bool == 1)
		buff = NULL;
	return (ret_str);
}
